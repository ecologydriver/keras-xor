# 必要なパッケージのロード
import numpy as np
from tensorflow.contrib.keras.python.keras.models import Sequential
from tensorflow.contrib.keras.python.keras.layers import Dense
from tensorflow.contrib.keras.python.keras.optimizers import SGD

# 入力Xと出力yを定義
X = np.array([[0,0],[0,1],[1,0],[1,1]])
y = np.array([[0],[1],[1],[0]])

#
# ニューラルネットワークの定義
#
model = Sequential()	# Sequentialモデル
# 入力2/出力8/活性化関数tanhの層を追加
model.add(Dense(8,input_dim=2, activation='tanh'))
# 入力8(省略)/出力1/活性化関数sigmoidの層を追加
model.add(Dense(1, activation='sigmoid'))
# 学習の仕方を定義
# * 目的関数(ロス関数)としてbinary_crossentropy(logloss)
# * 最適化アルゴリズムにSGD(確率的勾配降下法）、学習率を0.1に設定
# * 評価メトリクスとしてaccuracyを表示
model.compile(loss='binary_crossentropy',optimizer=SGD(lr=0.1),metrics=['accuracy'])

# 学習開始
# * バッチサイズ4
# * エポック数1000回
model.fit(X, y, batch_size=4, epochs=1000)

print(model.predict_proba(X)) 
